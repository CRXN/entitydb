#!/bin/bash

# Primitive testing script to check that the JSONs are valid.

if ! jq --version &> /dev/null; then
    echo "You need to install jq to run this script"
    exit 1
fi

ret=0

for file in *.json; do
	if ! jq --exit-status '.route | .[] | .device' < "$file" > /dev/null 2>&1
	then
		echo "$file is invalid." >&2
		ret=1
	else
		#echo "$file is valid."
		:
	fi
done

exit $ret

# entitydb

EntityDB holds all network allocations and associated information.

## Format

Every 'entity' has its data stored in a JSON file named as 'UNIQUE.json' where UNIQUE is any unique identifier (UUID/hash/name etc.).

### Content of entity (data)

The data should be a JSON object with a anonymous top-level object and "route" as only object. "route" should be a list of all routes provided by the entity with a required list of devices named "device".

"device" must contain at least one device that is reachable. A router is commonly used as the only object for the network as it is necessary in most setups.

Every device can contain the type of the device (router, generic, server), along with the list of services as "service". When no type is defined, generic is assumed.

Services must have a type (eg. tcp, udp, http, rsync, git) and at least one endpoint in the endpoints list.

Every object may contain a optional description, except lists.

Descriptions should not be used other than for describing things in a human-readable form.

### Example

Here's a example that you may modify to your needs.

```json
{
  "route": {
    "fdaa:bbcc:ddee::/48": {
      "description": "My home network",
      "device": {
        "fdaa:bbcc:ddee::1": {
          "type": "router",
          "description": "My custom router running BIRD"
        },
        "fdaa:bbcc:ddee::5": {
          "description": "Electronic mail endpoint",
          "service": [
            {
              "type": "smtp",
              "endpoint": [
                { "port": 25 }
              ]
            },
            {
              "description": "Super-secret authentication gateway",
              "type": "kerberos",
              "endpoint": [
                { "port": 89 }
              ]
            },
            {
              "type": "imap",
              "endpoint": [
                { "port": 143 }
              ]
            }
          ]
        }
      }
    },
    "fdcc:ba11:b00b::/48": {
      "device": {
        "fdcc:ba11:b00b::1": {
          "type": "router"
        }
      }
    }
  }
}
```

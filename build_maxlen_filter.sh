#!/usr/bin/bash

if ! jq --version &> /dev/null; then
    echo "You need to install jq to run this script"
    exit 1
fi

if [ ! "$1" ]; then
    echo "Please specify format: (babeld|babeld-uci|bird|yaml) [options]"
    echo ""
    echo "Options:"
    echo "babeld - [with-deny]"
    echo "babeld-uci  - [with-deny]"
    echo "bird - [prefix-list]"
    echo "yaml"
    exit 1
fi

filter=

for file in *.json; do
    [[ $file =~ ^(schema.json)$ ]] && continue
    for prefix in $(jq -r ".route | keys[]" "$file"); do
        prefixlen=$(echo "$prefix" | cut -d "/" -f2)
        maxlen=$(jq -r ".route.\"$prefix\".\"max-len\"" "$file")
        if [ "$maxlen" == "null" ]; then
            maxlen=64
        fi
        if [ "$1" == "bird" ]; then
            ending="{$prefixlen,$maxlen}"
            if [ "$2" == "prefix-list" ]; then
                ending=+
            fi
            filter="$filter$prefix$ending,\n"
        elif [ "$1" == "babeld" ]; then
            filter="${filter}in ip $prefix le $maxlen allow\n"
            if [ "$2" == "with-deny" ]; then
                filter="${filter}in ip $prefix deny\n"
            fi
        elif [ "$1" == "babeld-uci" ]; then
            filter="${filter}config filter\n\toption 'type'\t'in'\n\toption 'ip'\t'$prefix'\n\toption 'le'\t'$maxlen'\n\toption 'action'\t'allow'\n\n"
            if [ "$2" == "with-deny" ]; then
                filter="${filter}config filter\n\toption 'type'\t'in'\n\toption 'ip'\t'$prefix'\n\toption 'action'\t'deny'\n\n"
            fi
        elif [ "$1" == "yaml" ]; then
            filter="${filter}- prefix: $prefix\n  maxlen: $maxlen\n"
        fi
    done
done

if [ "$filter" == "" ]; then
    echo "Failed: Filter seems empty"
    exit 1
elif [ "$1" == "babeld-uci" ]; then
    filter=${filter::-4}
elif [ "$1" == "bird" ]; then
    filter=${filter::-3}
elif [ "$1" == "babeld" ] || [ "$1" == "yaml" ]; then
    filter=${filter::-2}
fi


echo -e "$filter"
